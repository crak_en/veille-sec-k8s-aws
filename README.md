# Veille sécurité AWS/K8S

## AWS :
  - Pentest:
    -  [ ] [https://github.com/dafthack/CloudPentestCheatsheets](https://github.com/dafthack/CloudPentestCheatsheets)
    -  [ ] [https://github.com/z0ph/aws-security-toolbox](https://github.com/z0ph/aws-security-toolbox)
  - Audits :
    -  [ ] [https://github.com/nccgroup/ScoutSuite](https://github.com/nccgroup/ScoutSuite)
    -  [ ] [https://github.com/bsd0x/awsreport](https://github.com/bsd0x/awsreport)
    -  [ ] [https://github.com/toniblyx/prowler](https://github.com/toniblyx/prowler)
    -  [ ] [https://www.guardicore.com/infectionmonkey/docs/setup/aws/](https://www.guardicore.com/infectionmonkey/docs/setup/aws/)
    -  [ ] [https://www.kitploit.com/2020/08/cloudsplaining-aws-iam-security.html](https://www.kitploit.com/2020/08/cloudsplaining-aws-iam-security.html)

## Kubernetes :
  - Guides :
    -  [ ] [https://deepsource.io/blog/setup-vault-kubernetes/?utm\_sq=gee67sqf3v](https://deepsource.io/blog/setup-vault-kubernetes/?utm_sq=gee67sqf3v)
    -  [ ] [https://sysdig.com/blog/image-scanning-best-practices/](https://sysdig.com/blog/image-scanning-best-practices/)
    -  [ ] [https://www.objectif-libre.com/fr/blog/2018/07/26/scanning-docker-images-with-clair-and-gitlab/](https://www.objectif-libre.com/fr/blog/2018/07/26/scanning-docker-images-with-clair-and-gitlab/)
    -  [ ] [https://cheatsheetseries.owasp.org/cheatsheets/Docker\_Security\_Cheat\_Sheet.html](https://cheatsheetseries.owasp.org/cheatsheets/Docker_Security_Cheat_Sheet.html)
    -  [ ] [https://medium.com/better-programming/kubernetes-vulnerabilities-in-2020-and-how-to-fix-them-6906f07ef10e](https://medium.com/better-programming/kubernetes-vulnerabilities-in-2020-and-how-to-fix-them-6906f07ef10e)
    -  [ ] [https://www.microsoft.com/security/blog/2020/04/02/attack-matrix-kubernetes/](https://www.microsoft.com/security/blog/2020/04/02/attack-matrix-kubernetes/)
    -  [ ] [https://medium.com/@deep75/kata-deploy-un-moyen-rapide-dinstaller-des-kata-containers-sur-son-cluster-kubernetes-b146158f6058](https://medium.com/@deep75/kata-deploy-un-moyen-rapide-dinstaller-des-kata-containers-sur-son-cluster-kubernetes-b146158f6058)
    -  [ ] [https://medium.com/swlh/secure-an-amazon-eks-cluster-with-iam-rbac-b78be0cd95c9](https://medium.com/swlh/secure-an-amazon-eks-cluster-with-iam-rbac-b78be0cd95c9)
    -  [ ] [https://realvz.github.io/awesome-eks/?utm\_sq=gg2ri78yf6](https://realvz.github.io/awesome-eks/?utm_sq=gg2ri78yf6)
      -  [ ] [https://aws.github.io/aws-eks-best-practices/](https://aws.github.io/aws-eks-best-practices/)
    -  [ ] [https://www.infracloud.io/kubernetes-pod-security-policies-opa/](https://www.infracloud.io/kubernetes-pod-security-policies-opa/)
    -  [ ] [https://thibault-lereste.fr/2020/04/kubernetes-popeye/](https://thibault-lereste.fr/2020/04/kubernetes-popeye/)



- Pentest workshop :
  -  [ ] [https://madhuakula.com/kubernetes-goat/](https://madhuakula.com/kubernetes-goat/); [https://katacoda.com/madhuakula/scenarios/kubernetes-goat](https://katacoda.com/madhuakula/scenarios/kubernetes-goat)
  -  [ ] [https://securekubernetes.com/?utm\_sq=gcb2duvl63](https://securekubernetes.com/?utm_sq=gcb2duvl63)

- Audits:
  -  [ ] [https://github.com/jasonrichardsmith/rbac-view](https://github.com/jasonrichardsmith/rbac-view)
  -  [ ] [https://github.com/liggitt/audit2rbac](https://github.com/liggitt/audit2rbac)
  -  [ ] [https://www.kitploit.com/2020/06/kube-bench-checks-whether-kubernetes-is.html](https://www.kitploit.com/2020/06/kube-bench-checks-whether-kubernetes-is.html)
  -  [ ] [https://github.com/appvia/krane](https://github.com/appvia/krane)
  -  [ ] [https://github.com/darkbitio/mkit?utm\_sq=gee50f1zcp](https://github.com/darkbitio/mkit?utm_sq=gee50f1zcp)
  -  [ ] [https://github.com/Portshift/Kubei](https://github.com/Portshift/Kubei)
  -  [ ] [https://github.com/cyberark/KubiScan?utm\_sq=gg856c1v7t](https://github.com/cyberark/KubiScan?utm_sq=gg856c1v7t)
  -  [ ] [https://github.com/controlplaneio/kubectl-kubesec](https://github.com/controlplaneio/kubectl-kubesec)
  -  [ ] [https://github.com/zegl/kube-score](https://github.com/zegl/kube-score)
  -  [ ] [https://thibault-lereste.fr/2020/04/kubernetes-popeye/](https://thibault-lereste.fr/2020/04/kubernetes-popeye/)
  -  [ ] [https://github.com/aquasecurity/kubectl-who-can](https://github.com/aquasecurity/kubectl-who-can)
  -  [ ] [https://github.com/team-soteria/rback](https://github.com/team-soteria/rback)

- Linters :
  -  [ ] [https://github.com/goodwithtech/dockle](https://github.com/goodwithtech/dockle)
- Autres outils &amp; frameworks :
  -  [ ] [https://katacontainers.io/docs/](https://katacontainers.io/docs/)
  -  [ ] [https://rbac.dev/](https://rbac.dev/)
  -  [ ] [https://falco.org/#whyfalco](https://falco.org/#whyfalco)
  -  [ ] [https://akomljen.com/kubernetes-backup-and-restore-with-velero/](https://akomljen.com/kubernetes-backup-and-restore-with-velero/)
  -  [ ] [https://toolbox.kali-linuxtr.net/kubernetes-rbac-permission-manager.tool](https://toolbox.kali-linuxtr.net/kubernetes-rbac-permission-manager.tool)
  -  [ ] [https://anchore.com/kubernetes/](https://anchore.com/kubernetes/)
  -  [ ] [https://github.com/sysdiglabs/kube-psp-advisor](https://github.com/sysdiglabs/kube-psp-advisor)



## Terraform :
-  [ ] [https://github.com/liamg/tfsec](https://github.com/liamg/tfsec)
-  [ ] [https://github.com/bridgecrewio/checkov](https://github.com/bridgecrewio/checkov)

## Autres :
  - Vulnérabilités Gitlab : [https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=gitlab](https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=gitlab)